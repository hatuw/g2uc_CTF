# g2uc_CTF writeup
---

## 送命题
**1. 邮箱（10'）**

g2uc协会的邮箱是？

提交形式：g2uc_CTF{xxxxxx}

送分题，`g2uc_CTF{g2uc170308@sina.com}`


**2. 天王盖地虎（301'）**

根据题目图片：

![河妖](https://gitee.com/uploads/images/2017/1106/174135_b13916bd_1219519.jpeg "河妖.jpg")

百度识图搜一下，是傅立叶，联想到傅立叶变换之类的，先不管。

图片题先考虑隐写，解出来有一张图片，一个不知道是什么的文件。(分析一下可以知道是Matlab数据文件)

至此我们得到了两张图片和一个数据文件，对两张图片进行傅立叶变换，再相减看看。根据题目提示，天王盖地虎，对应宝塔->镇->河妖

…………em..好吧扯不下去了。其实就是基于频域的数字水印而已，Matlab数据文件是加水印的时候将水印打乱，解水印的时候根据对应的位置还原就好了。直接上poc，看不懂的同学可以去看看相关的论文

``` Matlab
%%
close all;
clear;
clc;

%% extract watermark
alpha = 10;
wmpath = './wm.bmp';
source_im = './source.jpg';
im = double(imread(wmpath))/255;
source_img = double(imread(source_im))/255;
imsize = size(im);

FA = fft2(source_img);

load('encode.mat')
%FAO = im;
FA2 = fft2(im);
G=1.0-abs((FA2-FA)/alpha);
GG=G;
for i=1:imsize(1)*0.5
    for j=1:imsize(2)
        GG(M(i),N(j),:)=G(i,j,:);
    end
end
for i=1:imsize(1)*0.5
    for j=1:imsize(2)
        GG(imsize(1)+1-i,imsize(2)+1-j,:)=GG(i,j,:);
    end
end
figure,imshow(GG);title('extracted watermark');
```

当然有兴趣的同学也可以用Python复现一遍，用`scipy.io`就可以load`.mat`文件了，解出来大概是这个样子：

![flag](https://gitee.com/uploads/images/2017/1106/174835_435a26f7_1219519.jpeg "flag.jpg")

> g2uc_CTF{A_easy_fft_steg}

---
## Reverse
**1. 初学者（150'）**

将文件下载下来后，我们首先得要了解一下逆向必备的两个工具，ida和ollydbg，我们用ida32打开

![输入图片说明](https://gitee.com/uploads/images/2017/1101/235537_b95b036e_1611313.png "屏幕截图.png")

简单看一下这段汇编代码，就是说把输入的username和password进行比较，test后，如果zf(零标志位)不等于0则跳到short loc_4017A6这函数

![输入图片说明](https://gitee.com/uploads/images/2017/1101/235601_b6f79cfd_1611313.png "屏幕截图.png")

也就是用户名和密码要相同才会跳到正确flag的那一部分。还有一种做法就是直接使用ida快捷键：ctrl+alt+T，搜索flag，就能看到flag了。是不是so easy。

>g2uc_CTF{g_2_u_c1234}


**2. kaisa（300'）**

- 太懒不想看版本

> g2uc_CTF{FKaiSaqeto}

- 陈熙大佬版本

第一步，查壳，这里用的是Detect it Eas

![Detect it Eas结果](https://gitee.com/uploads/images/2017/1106/175210_31875853_1219519.png "屏幕截图.png")

可以看到没有加壳，是一个标准的，使用gcc编译的win32控制台c程序

可以直接用[在线反编译网站](https://retdec.com/decompilation/)载入KaiSa.exe，点击Decompile进行反编译（但似乎反编译的结果有点问题，目前不是很推荐，第一题倒是可以用），或者用IDA Pro载入程序，按F5反编译成c代码（推荐）

![在线反编译网站](https://gitee.com/uploads/images/2017/1106/175239_12ac8de9_1219519.png "屏幕截图.png")

![retdec.com反编译结果](https://gitee.com/uploads/images/2017/1106/175249_2762d547_1219519.png "屏幕截图.png")

![IDA反编译结果](https://gitee.com/uploads/images/2017/1106/175323_f17e990f_1219519.png "屏幕截图.png")

两个我都看了下，两个的可读性都不错。我们这里先用IDA Pro 来讲，因为网站的反编译结果似乎有一些问题。IDA是个反编译界的大杀器，想研究的童鞋可以自己研究一下（因为我也不会用哈哈哈）

先来看它的main函数部分

![main函数](https://gitee.com/uploads/images/2017/1106/175407_a4b74be1_1219519.png "屏幕截图.png")

其实前面很大一部分都是没有用的，负责初始化，有用的代码从18行开始。18行输出一开始的"g2uc_CTF{flag}"，19行改变背景颜色为紫色，20到21行输出打印这两行东西

![inputflag](https://gitee.com/uploads/images/2017/1106/212257_7f52d320_1219519.png "屏幕截图.png")

所以这些代码其实也是没用的哈哈哈

25行scanf开始就有用咯，25行输入flag，26行HandleInput函数处理你输入的flag，我们就从这里钻进去。

双击HandleInput函数进入

![转换](https://gitee.com/uploads/images/2017/1106/212736_f81336e2_1219519.png "屏幕截图.png")
![转换](https://gitee.com/uploads/images/2017/1106/212329_1b9471c4_1219519.png "屏幕截图.png")

可以看到开始是几个int变量v2~v7。实际上这里是将文本用hex（16进制）的方式存储在了变量里，这样在反编译的时候不会显示出明文。

比如，v4 = 0x67616c66 (1734437990是十进制表示)，但存储的时候是反过来的，所以实际的文本应该是 66 6c 61 67，转成文本，就是flag。你可以在数字这里右键，点char，就会显示成字符（如上图）

这个问题讲完了，可以继续往下看了

![forloop](https://gitee.com/uploads/images/2017/1106/212353_b8b9e099_1219519.png "屏幕截图.png")

`for ( i = 0; i <= 9; ++i );`等价于i=10

``` c
if ( a1[i] )
    *a1 = 119;
if ( *a1 == 70 )
{
    result = memcmp(a1 + 1, &v2, 5u);
    if ( !result )
        result = decode((int)(a1 + 6));
}
```

这一段会有些略难懂，实际上就是判断输入的字符串的第11位是不是null（0），就是说判断输入的字符是否在10位以内。

如果第十一位不是0呢，if判断就会成立，*a1 = 119;意思是把第一个字符改成字符119(w)，这样的话下一个判断 *a1 == 70 就不可能成立，也就不会进入接下来的判断。而if下面的else里就是一些与正确答案无关的代码了，把KaiSa进行了一下移位，然后输出flagwrong

![与wrongflag有关的一些奇怪代码](https://gitee.com/uploads/images/2017/1106/212419_6d1845ed_1219519.png "屏幕截图.png")

继续看if里面，`if (*a1 == 70)`，判断第一个字符是否为F（asc码70为F）

也就是说我们的flag第一个字符为F，奈斯

继续往下看

![memcmp](https://gitee.com/uploads/images/2017/1106/212428_febb7355_1219519.png "屏幕截图.png")

Memcmp，通过百度可以知道是一个用于比较的函数

![Memcmp](https://gitee.com/uploads/images/2017/1106/212440_f59c3255_1219519.png "屏幕截图.png")

比较的内容呢，是a1+1（即输入的字符的第二个开始）和v2（加v3，连起来就是KaiSa），比较的位数是5。为什么是v2+v3 KaiSa而不是KaiS呢，不用想太多，因为v2v3在内存里是连起来的而且memcmp比较的位数是5。

比较后判断result是不是0.当两个相等时memcmp的返回值是0.而!result时if才能成立的话result就得是0。所以你输入的第二位到第五位要和v2+v3相等
很清晰了，也就是说看你输入的flag第2到6位一定得是KaiSa。

也就是说我们现在的flag是FKaiSa了。

当比较成功后就进入了

![decode](https://gitee.com/uploads/images/2017/1106/212451_cd25b653_1219519.png "屏幕截图.png")

也就是说该进入decode函数了，输入的参数是我们输入flag 的第7位以后。前面我们说字符位数要在10位以内，所以这里实际上就是输入了最后4位

![decode](https://gitee.com/uploads/images/2017/1106/212503_d23eb526_1219519.png "屏幕截图.png")

第5行的for实际上就是遍历a1的每一个字符，遍历了之后里面实际上就是个凯撒加密，先-97是因为asc码的小写字母是从97开始的，-97就能把字母变成1到26，整体上移3个字母后再变回asc码，然后调用check函数，参数是我们凯撒加密过的这4个字符。

我们暂时看不出这四个字符是什么，所以我们进check里看看。

![check](https://gitee.com/uploads/images/2017/1106/212516_e8415a09_1219519.png "屏幕截图.png")

V6到v4我已经帮你们翻译好了，同样也都是hex形式存储的文本

直接看if判断:

第一个*(_BYTE *)a1 != 0x74     判断第一位是不是0x74 ：t

第二个 *(_BYTE *)(a1 + 1) != HIBYTE(v6) 判断第二位是不是等于HIBYTE(v6)，这个我查了一下，HIBYTE()得到一个16bit数最高（最左边）那个字节，也就是0x68，就是字母h

第三个 *(_WORD *)(a1 + 2) != *(_WORD *)((char *)&v3 + 1) 判断第3位和v3的第二位。注意这里是_WORD类型，百度可以知道 一个word=两个byte ，也就是说最后一个判断比较的是两位，就是wr两个字符（注意这里是v3+1，就是代表从72开始，就是0x7277，反过来就是0x7772，wr）

所以最后四位也出来了，thwr，但flag就是FKaiSathwr了吗？当然不是。

上一步decode还做了个凯撒加密，所以你输入的flag如果要跑通的话得把thwr后移3位，就是qeto，这样你输入时候才能跑通

所以一个完整的flag就出来啦，FKaiSaqeto

输入程序里试一下

![flag](https://gitee.com/uploads/images/2017/1106/212534_7d197e8e_1219519.png "屏幕截图.png")

果然正确了。所以最后提交的答案就是`g2uc_CTF{FKaiSaqeto}`。

其实如果会用OD的话最后四个字符用OD解是很简单的，直接可以在内存里看到程序是在一个字符一个字符的对比，直接就可以把4个字符在内存里弄出来。

---
## Misc
**1. 64esab（40'）**

题目是一个倒过来的base64，说明这个编码在编码某个环节可能有个逆序的过程

根据base64的编码原理，能够逆序的可以有以下可能

原文逆序

编码映射表逆序（即’A’=>63, ‘B’=>62, ...,’+’=>1, ‘/’=>0）

编码文本逆序

首先根据第三种思路尝试最后得到的是乱码，放弃

然后根据第二种思路尝试

1.手动计算

这个只要按照解码过程计算，把编码表反过来就行

2.修改在线工具的编码表

随便找个支持在线解的网站，这里以[http://www1.tc711.com/tool/BASE64.htm](http://www1.tc711.com/tool/BASE64.htm)为例

解码按钮那里用审查元素，找到

![输入图片说明](https://gitee.com/uploads/images/2017/1106/113804_592c32e1_1617818.png "图片1.png")

发现onclick触发了doDecode();

用Ctrl+Shift+F搜索这个函数，发现这个函数存在于Base64.js里

![输入图片说明](https://gitee.com/uploads/images/2017/1106/113855_4c9628b2_1617818.png "图片1.png")

浏览整个文件，发现顶部有两个数组

![输入图片说明](https://gitee.com/uploads/images/2017/1106/113956_a6850619_1617818.png "图片1.png")

第二个就是base64代码的解码表

接下来只要用63减去里面大于-1的数字即可完成逆序
需要在Console窗口运行

![输入图片说明](https://gitee.com/uploads/images/2017/1106/114130_1eeefffa_1617818.png "图片1.png")

最后再点击页面的解码按钮得出flag

>g2uc_CTF{g8D_Job}


**2. 签到题（50'）**

ZzJ1Y19DVEZ7V2UxYzBtZV90MF9nMnVjX0M3RiF9

提交形式： g2uc_CTF{xxx}

咋一看没啥规律，尝试一下base64 decode~

``` python
import base64
print base64.b64decode('ZzJ1Y19DVEZ7V2UxYzBtZV90MF9nMnVjX0M3RiF9')
```
> output: g2uc_CTF{We1c0me_t0_g2uc_C7F!}


**3. 猜猜（50'）**

将文件下载下来，

![输入图片说明](https://gitee.com/uploads/images/2017/1101/235639_13a6c904_1611313.png "屏幕截图.png")

首先查看一下他的属性，然后就能看到答案了

![输入图片说明](https://gitee.com/uploads/images/2017/1101/235650_feed7427_1611313.png "屏幕截图.png")

> g2uc_CTF{哈哈没想到吧}


**4. Basic Hex（50'）**

第一眼看上去以为这里是表达式，但是留意题目上的basic，可能在提示是base64编码，因为数字、加号、等号在base64编码里面是合法的

利用工具解出来发现是乱码，一般不会用乱码作为flag的，这时留意Hex，可以考虑把乱码的ASCII转化成十六进制的形式再提交

![输入图片说明](https://gitee.com/uploads/images/2017/1106/114951_46b9c2aa_1617818.png "QQ截图20171106114956.png")

最后加上提交格式提交

> g2uc_CTF{d7ed7edbedfee7ef3ed77fb6d7edf8fb9e}


**5. Eassy_password（60'）**

国庆节生日的红红注册了一个新的qq，为了方便记忆，她以她名字的拼音和生日组成了qq密码，猜猜她qq密码是多少?

提交格式：g2uc_CTF{她的qq密码}。

> g2uc_CTF{honghong1001}


**6. 你猜猜哈哈（100'）**

`M4ZHKY27NBQWQYLIMFUGC===`

看到这段话应该想到是base64或者是base32，然后我们尝试一下base64，发现不行，然后我们试一下base32,答案出来了。

``` python
import base64
print base64.b32decode("M4ZHKY27NBQWQYLIMFUGC===")
```
>g2uc_CTF{g2uc_hahahaha}

**7. 不存在的（100'）**

下载题目，打开是一张图片

![题目](https://gitee.com/uploads/images/2017/1106/212754_7f083933_1219519.png "屏幕截图.png")

首先自然要考虑有没有隐写，直接尝试把后缀改成zip能不能打开

![zip](https://gitee.com/uploads/images/2017/1106/212822_dfcdaf47_1219519.png "屏幕截图.png")

果然可以。也可以先用binwalk等程序来检查是否有隐写，然后提取出里面的压缩包等等，这里就不写的这么复杂了。

根据题目提示（不存在的）以及图片（滑稽），以及压缩包里就一个answer，可以判断这基本上就是个伪加密的题而已了（其实是因为这道题是我出的hh）。有关zip伪加密可以自行百度一下就懂了，也就是说这个zip实际上并没有加密，简单改一下就能解压，在一些系统或压缩软件上甚至直接就能解压。

打开winhex，载入1.zip，直接拉到最底下，把0xBD37位的09改成00，保存，解压就可以看到flag了。

![winhex](https://gitee.com/uploads/images/2017/1106/212847_72183f9e_1219519.png "屏幕截图.png")

![flag](https://gitee.com/uploads/images/2017/1106/212856_7532b8f2_1219519.png "屏幕截图.png")

> g2uc_CTF{tingshuomimachangyidianbijiaohao}

**8. Easy Steg（150'）**

小明很懒，什么也没留下

提交形式：g2uc_CTF{xxxxxx}

图片：

![flag.jpg](./assets/flag.jpg)

首先考虑图片隐写，用unzip解压后得到一个pdf文件，flag藏在里面(白色字体)，全选就能看到了(或者复制出去)。

> g2uc_CTF{y0u_f0und_it!}


**9. 王牌特工不错（250'）**

将图片下载后，查看属性，发现没什么有用的信息，然后我们拖进kali里用binwalk查看一下

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000204_2406bdaa_1611313.png "屏幕截图.png")

我们可以发现里面是有一个zip的，使用命令binwalk -e 3333.jpg将zip提取出来，发现这是一个加密了的zip。我们可以尝试使用zip的爆破工具，譬如Ziperello，然后爆出来密码是111111.然后我们打开压缩包，发现里面还有一张图片和tip.txt。Tip里面写着passphrase:g2uc。看到passphrase这关键词，我们可以百度一下

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000217_a0e526c7_1611313.png "屏幕截图.png")

很有可能是使用了steghide进行隐写。然后我们百度一下steghide怎么用的。

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000225_0e50a913_1611313.png "屏幕截图.png")

然后我们可以看到里面是有一个g2uc.txt的文件的，我们把他提取出来就好了。

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000238_8be11ae5_1611313.png "屏幕截图.png")

> g2uc_CTF{答案就在这里}


---
## Web
**1. 没想到吧!（50'）**

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000531_cead5245_1611313.png "屏幕截图.png")

这其实是jsfuck，只要复制进f12控制台就可以得到flag了

> g2uc_CTF{我就是flag}


**2. Hello Web（50'）**

查看网页源代码，flag就隐藏在form里面（表单是伪装的，没用）

> g2uc_CTF{gay里gay气的}

**3. 哇（70'）**
```
$=~[];$={___:++$,$$$$:(![]+"")[$],__$:++$,$_$_:(![]+"")[$],_$_:++$,$_$$:({}+"")[$],$$_$:($[$]+"")[$],_$$:++$,$$$_:(!""+"")[$],$__:++$,$_$:++$,$$__:({}+"")[$],$$_:++$,$$$:++$,$___:++$,$__$:++$};$.$_=($.$_=$+"")[$.$_$]+($._$=$.$_[$.__$])+($.$$=($.$+"")[$.__$])+((!$)+"")[$._$$]+($.__=$.$_[$.$$_])+($.$=(!""+"")[$.__$])+($._=(!""+"")[$._$_])+$.$_[$.$_$]+$.__+$._$+$.$;$.$$=$.$+(!""+"")[$._$$]+$.__+$._+$.$+$.$$;$.$=($.___)[$.$_][$.$_];$.$($.$($.$$+"\""+$.$_$_+(![]+"")[$._$_]+$.$$$_+"\\"+$.__$+$.$$_+$._$_+$.__+"(\\\""+$.$__+$.$_$_+$.$$__+$.$$$+$.$__+$.$$_$+$.$$$$+$.$$$$+$.$_$$+$._$$+$.__$+$.__$+$.$$$_+$.$$$+$.$_$+$.$$$_+"\\\")"+"\"")())();
```

打开后发现是一堆不明觉厉的东西，这其实是Jjencode。这也是javascript用自定义符号混淆的一种方式，同样把他放到f12控制台，

![输入图片说明](https://gitee.com/uploads/images/2017/1102/000705_a756929b_1611313.png "屏幕截图.png")

可以得到4ac74dffb311e75e。这个16位的数我们可以尝试一下去cmd5解一下，然后就能得到答案了。

> g2uc_CTF{g2uc}


**4. F12（100'）**

从代码里头var开头可以猜测可能是js代码，题目提示F12，可以考虑在浏览器的开发者工具的Console窗口运行一下

![输入图片说明](https://gitee.com/uploads/images/2017/1106/115348_261e1507_1617818.png "图片1.png")

> g2uc_CTF{Ea5y-C0n501e}

---
## Crypto
**1. Easy morse（50'）**

嘀嗒嘀嗒嘀嗒嘀嗒 时针它不停在转动

`--  ---  .-.  ...  .`

嘀嗒嘀嗒嘀嗒嘀嗒 小雨它拍打着水花

`-.-.  ---  -..  .`

提交形式：g2uc_CTF{xxxxxx}

[莫斯密码](http://www.zhongguosou.com/zonghe/moErSiCodeConverter.aspx)，在线转换一下得到MORSECODE，提交`g2uc_CTF{MORSECODE}`即可。

**2. Classical（50'）**

`gCri_tp2T@0c0huFdnrgyc{i@yr}_Tt1p@@`

提交形式：g2uc_CTF{xxxxxx}

尝试使用栅栏密码解密，当栏数为5时解密得到：

> g2uc_CTF{Tr@diti0n@1_crypt0gr@phy}@

跟实际flag相似，去掉最后的`@`提交即可。

**3. 这个呢哈哈（50'）**

`zpv_bsf_sjhiu`

提交格式：g2uc_CTF{xxxxxx}

考虑凯撒移位，得到其中一个移位结果为：

`you_are_right`

提交`g2uc_CTF{you_are_right}`即可。

**4. 滴答滴答（50'）**

`**/*-**/---/***-/*/--*/**---/**-/-*-*/-*-*/-/**-*/`

提交格式：g2uc_CTF{xxxxxx}

还是[莫斯密码](http://www.zou114.com/mesm/)，翻译得到`iloveg2ucctf`，提交`g2uc_CTF{iloveg2ucctf}`即可。


**5. 调皮的小明（60'）**
有一次小明同学入侵了学校的教务系统，并获得了管理员的密码，但是他一不小心修改了一下密码，他很烦恼，现在我们来帮帮小明同学还原管理员密码吧。

`f5c005fc42a096fl052751d9aee612612`

我们可以发现这段密码很像md5。但是却有33位。根据上下文我们可以知道应该是多了一个字符，我们可以百度一下md5，发现md5里所含的字母范围是a-f。然而这里出现了个`l`，应该把它删掉。然后再去cmd5解密就好了。

> g2uc_CTF{gzdx}


**6. 又是密码学哈哈哈（100'）**

`%7d3e0do31sv_eo%40_1%40o0juje%40sU%7bGUD_dv2h`

提交格式：g2uc_CTF{xxxxxx}

首先考虑urldecode，得到：

`}3e0do31sv_eo@_1@o0juje@sU{GUD_dv2h`

倒序，得到：

`h2vd_DUG{Us@ejuj0o@1_@oe_vs13od0e3}`

接下来就比较明显是移位了，列出所有组合，得到其中一个为：

`g2uc_CTF{Tr@diti0n@1_@nd_ur13nc0d3}`

提交即可。